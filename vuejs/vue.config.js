module.exports = {
  outputDir: '../www',
  baseUrl: '',
  assetsDir: undefined,
  runtimeCompiler: true,
  productionSourceMap: undefined,
  parallel: undefined,
  css: undefined
}
