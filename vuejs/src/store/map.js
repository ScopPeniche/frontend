import * as turf from '@turf/helpers'
// On définit les types de mutations possibles
const MAP_DATA = 'MAP_DATA'

// les données qu'on souhaite utiliser / modifier
const state = {
  mapData: [],
}

// les getters pour avoir accès à ces données
const getters = {
  mapData: state => state.mapData,
}

// les fonctions pour modifier ces données
const mutations = {
  [MAP_DATA]: (state, response) => {
    state.mapData = response
  }
}

// les fonctions qui vont déclancher les mutations
const actions = {
  async getData({ commit }) {
    try {
      const features = []
      let response = await axios.post('collect_points')
      for (let key in response.data.data) {
        let feature = turf.point([response.data.data[key].lon_x, response.data.data[key].lat_y])
        feature.properties['address'] = response.data.data[key].address
        feature.properties['city'] = response.data.data[key].city
        feature.properties['waste_type'] = response.data.data[key].waste_type
        feature.properties['container_type'] = response.data.data[key].container_type
        features.push(feature)
      }
      commit(MAP_DATA, features)
    } catch (error) {
      console.log(error)
    }
  }
}
export default {
  state,
  getters,
  mutations,
  actions
}