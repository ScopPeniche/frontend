import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import account from './account'
import waste from './waste'
import map from './map'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    account,
    waste,
    map
  }
})