import axios from 'axios'
import store from './store'

const BASE_URL = process.env.VUE_APP_URLBACK

window.axios = axios
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
axios.defaults.crossDomain = true
axios.defaults.baseURL = `${BASE_URL}/api/v1/`

const token = localStorage.getItem('token')
if (token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
}
// Intercepteur de la requête pour rafraichir le token ou se déconnecter
axios.interceptors.response.use(
  function (response) {
    return response
  }, 
  function (error) {
    const originalRequest = error.config
    if (error.response.status === 401 && error.response.data.message == "Token has expired") {
      originalRequest._retry = true
      return axios.post('auth/refresh')
        .then(resp => {
          localStorage.setItem('token', resp.data.access_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + resp.data.access_token
          originalRequest.headers['Authorization'] = 'Bearer ' + resp.data.access_token
          store.commit('AUTH_SUCCESS', resp)
          return axios(originalRequest)
        }).catch((error) => {
          store.dispatch('AUTH_LOGOUT').then(() => {
            router.push({ name: 'Login' })
          }).catch(() => {
            router.push({ name: 'Login' })
          })
        })
    }
    return Promise.reject(error)
})